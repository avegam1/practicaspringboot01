package com.crud.democrud.models;

import javax.persistence.*;

@Entity
@Table(name = "usuariorol")
public class UsuarioRol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private  long idUsuario;

    private String idRol;
    private String Rol;

    public UsuarioRol(long idUsuario, String idRol, String rol) {
        this.idUsuario = idUsuario;
        this.idRol = idRol;
        Rol = rol;
    }
    public UsuarioRol() {

    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdRol() {
        return idRol;
    }

    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }

    public String getRol() {
        return Rol;
    }

    public void setRol(String rol) {
        Rol = rol;
    }
}
