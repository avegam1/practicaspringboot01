package com.crud.democrud.controllers;


import com.crud.democrud.models.UsuarioModel;
import com.crud.democrud.models.UsuarioRol;
import com.crud.democrud.services.UsuarioRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/usuariorol")
public class UsuarioRolController {
    @Autowired
    UsuarioRolService usuariorolService;

    @GetMapping(path = "/{idUsuario}")
    public Optional<UsuarioRol> obtenerUsuarioRolPorId(@PathVariable("idUsuario") Long idUsuario) {
        return this.usuariorolService.obtenerPorId(idUsuario);
    }
    @PutMapping (path = "/{idUsuario}")
    public UsuarioRol updateUsuarioRol(@RequestBody UsuarioRol usuariorol) {
        return this.usuariorolService.UpdateUsarioRol(usuariorol);
    }
}
