package com.crud.democrud.repositories;

import com.crud.democrud.models.UsuarioModel;
import com.crud.democrud.models.UsuarioRol;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRolRespository  extends CrudRepository<UsuarioRol, Long> {

}
