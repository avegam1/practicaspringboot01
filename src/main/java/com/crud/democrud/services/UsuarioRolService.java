package com.crud.democrud.services;

import com.crud.democrud.models.UsuarioModel;
import com.crud.democrud.models.UsuarioRol;
import com.crud.democrud.repositories.UsuarioRolRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioRolService {
    @Autowired
    UsuarioRolRespository usuarioRolRespository;
    public UsuarioRol UpdateUsarioRol( UsuarioRol usuariorol){
        return usuarioRolRespository.save(usuariorol);
    }
    public Optional<UsuarioRol> obtenerPorId(Long idUsuario){
        return usuarioRolRespository.findById(idUsuario);
    }
    /*public void UpdateUsarioRol(UsuarioRol usuariorol){
         usuarioRolRespository.save(usuariorol);
    }*/
   /* public UsuarioModel guardarUsuario(UsuarioModel usuario){
        return usuarioRepository.save(usuario);
    }*/
}
